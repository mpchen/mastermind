#!/usr/bin/env python

import json
import random
import rfid
import select
import socket
import string
import sys
import time

from display import \
    display_new_results, \
    display_game_done, \
    display_new_scan, \
    display_set_up, \
    display_round
################ Constants
player_rfids = {1: '-1654745749839463421', 2: '-3967869681041280378', 3: '-3678859335126938773', 4: '-3678859335121938767', 5: '-3678859335132938789', 6: '-3678859335130938797', 7: '-3678859335118938757', 8: '-3678859335119938755', 9: '-3678859335131938787', 10: '-382765223942188013', 11: '-1654760749908463167', 12: '-1382956224969189271', 13: '4123307705390202125', 14: '-8130401520978281647'}
NEW_SCAN_DELAY = 10

NUM_PLAYERS = 5

HOST_IP = "10.0.1.2" # Get from server
HOST_IP = "0.0.0.0"
HOST_PORT = 2525
RECV_BUFFER = 4096

################ Gameplay related stuff
# Types of strings:
#   server->client:
#      -- [] - Order of correct players for each client (once a round)
#      -- "KEEP_SCANNING" - Display result and scan next
#      -- "GAME_DONE" - Display finished
#   client->server:
#      -- bool - If all players scanned correctly at this client
scanned = []
correct = []
displayed_results = []
next_results = []
round_number = -1
have_scan_delay = False
scan_delay = NEW_SCAN_DELAY

def show_new_results():
    global displayed_results, next_results
    # TODO: Finish this
    print "new", next_results, "old", displayed_results
    displayed_results = list(next_results)
    print "new results: ", displayed_results
    display_new_results(displayed_results)

def keep_scanning():
    global scanned, scan_delay, have_scan_delay
    show_new_results()
    scanned = []
    scan_delay = NEW_SCAN_DELAY
    have_scan_delay = True 

def start_new_round(msg):
    print "New round"
    global correct, round_number, scanned
    global have_scan_delay
    show_new_results()
    correct = msg
    round_number += 1
    scanned = []
    display_round(round_number) 
    have_scan_delay = True
    scan_delay = NEW_SCAN_DELAY

# Parse message from server
def parse_from_server(msg):
    print "msg from server", msg
    # Change the display (because no new round)
    if msg == "KEEP_SCANNING":
        keep_scanning() 
        return
    if msg == "GAME_DONE":
        display_game_done()
        return  
    # New round about to start
    start_new_round(msg)
    
# Determine what the next set of results should be based on what has been
# scanned
def check_correct():
    global round_number, round_passable, next_results
    next_results = [[False, False, False] for _ in range(NUM_PLAYERS)] 
    round_passable = True
    print correct
    for i in range(NUM_PLAYERS):
        # Determine the real color for this position
        if scanned[i] in correct:
            if scanned[i] == correct[i]:
                next_results[i][0] = True   # Green
            else:
                next_results[i][1] = True   # Yellow
        else: 
            next_results[i][2] = True       # Red
    
        round_passable &= next_results[i][0]

        # Add some amount of fuzz (comment to remove)
        threashhold = float(round_number) / 10.0
        if random.random() < threashhold:
            next_results[i][0] |= True
        if random.random() < threashhold:
            next_results[i][1] |= True
        if random.random() < threashhold:
            next_results[i][2] |= True
    print round_passable
    send_to_server(round_passable)

def add_new_scan(msg):
    scanned.append(msg)
    display_new_scan(len(scanned))

# Parse message from the stackers scanning themselves in
def parse_new_scan(msg):
    # If the game hasn't started yet, don't scan
    if round_number < 0:
        print "Not scanning; game not yet started."
        return
    # Ignore any new scans if we've already got enough unique scans
    if len(scanned) == NUM_PLAYERS:
        print "Ignoring new scans; we have enough players."
        return
    # Otherwise, see if the new scan is unique and should be added to 
    # list of scanned players at this terminal.
    if msg not in scanned:
        print "Adding new scan"
        add_new_scan(msg)
        if len(scanned) == NUM_PLAYERS:
            check_correct()

def notify_server_of_num_players():
    send_to_server(NUM_PLAYERS)


################ Message sending code
s = ''

def send_to_server(msg):
    try: 
        s.send(json.dumps(msg))
        print "client sent message: ", msg
    except:
        print "Could not sent to server..."

def be_client():
    global s
    global have_scan_delay, scan_delay
    rfid_reader = rfid.RFIDReader()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.settimeout(2)

    # Try to connect to a server
    try: 
        s.connect((HOST_IP, HOST_PORT))
    except: 
        print "Unable to connect to ", HOST_IP, ":", HOST_PORT
        sys.exit()
    print "Connected"

    notify_server_of_num_players()

    while True:
        socket_list = [sys.stdin, s]
        read_sockets, write_sockets, error_sockets = \
                select.select(socket_list, [], [], 0.1)
        for sock in read_sockets:
            # message from server
            if sock == s:
                try:
                    data = sock.recv(RECV_BUFFER)
                    if data == '':  # Blank gets sent during connection
                        continue
                    parse_from_server(json.loads(data))
                except Exception, err:
                    print Exception, err
                    print "Got exception in client"
                    print sock
                    sys.exit()
        # TODO: Get message from RFID instead of stdin
        msg = rfid_reader.read()
        if have_scan_delay:
            time.sleep(0.05)
            scan_delay -= 1
            if scan_delay < 0:
                have_scan_delay = False
            continue
        print have_scan_delay
        if msg is not None and msg in player_rfids.values() and not have_scan_delay:
	    parse_new_scan(msg)

    s.close()

#### MAIN LOOP        
if len(sys.argv) != 2:
    print "Usage: client.py [number of stackers at client]"
    sys.exit()
NUM_PLAYERS = int(sys.argv[1]) 
displayed_results = [[False, False, False] for _ in range(NUM_PLAYERS)] 
display_set_up(NUM_PLAYERS)
be_client()
