# RFID reader library
# alex@aroper.net

# NOTE: beep is currently fucky. Don't use it -- we don't know enough about the
# protocol so it can mess up reads.

import os
import serial

TIMEOUT = 0.001
BAUD_RATE = 38400
UPPER_BOUND_ON_TAG_LENGTH = 256

READ_COMMAND = '\xaa\xdd\x00\x03\x01\x0c\x0d'

NO_CARD_VALUE = '\xaa\xdd\x00\x04\x01\x0c\x01\x0c'

class NoDeviceException(Exception):
    pass


def _init_device(candidate):
    tty = serial.Serial(candidate)
    tty.setTimeout(TIMEOUT)
    tty.setBaudrate(BAUD_RATE)
    return tty

def _guess_device():
    for prefix in ('USB', 'ACM'):
        for num in xrange(10):
            candidate = '/dev/tty%s%i' % (prefix, num)
            if os.path.exists(candidate):
                try:
                    return _init_device(candidate)
                except Exception:
                    pass
    else:
        raise NoDeviceException

class RFIDReader(object):
    def __init__(self, path=None):
        if path is None:
            self._tty = _guess_device()
        else:
            self._tty = _init_device(path)

    def read(self):
        try:
            self._tty.write(READ_COMMAND)
            value = self._tty.read(UPPER_BOUND_ON_TAG_LENGTH)
            if value == '':
                # This is an error condition. Dont' assert in prod:P
                return None
            elif value == NO_CARD_VALUE:
                return None
            else:
                return str(hash(value))
        except Exception:
            return None
