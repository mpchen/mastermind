#!/usr/bin/env python

import socket
import select
import sys
import random 
import json

############### Important constants 
ROUNDS_TOTAL = 3
NUM_PLAYERS = 10
NUM_COMPUTERS = 3

PORT = 2525
RECV_BUFFER = 4096
player_dict = {1: '-1654745749839463421', 2: '-3967869681041280378', 3: '-3678859335126938773', 4: '-3678859335121938767', 5: '-3678859335132938789', 6: '-3678859335130938797', 7: '-3678859335118938757', 8: '-3678859335119938755', 9: '-3678859335131938787', 10: '-382765223942188013', 11: '-1654760749908463167', 12: '-1382956224969189271', 13: '4123307705390202125', 14: '-8130401520978281647'}
################ Gameplay related stuff
# Types of strings:
#   server->client:
#      -- [] - Order of correct players for each client (once a round)
#      -- "KEEP_SCANNING" - Display result and scan next
#      -- "GAME_DONE" - Display finished
#   client->server:
#      -- int - Number of players that can be scanned at this terminal
#      -- bool - If all players scanned correctly at this client 
num_client_players = {}
client_msgs_recieved = 0
round_number = -1
round_passable = True

def display_game_can_start():
    print "game can start (clients all scanned)"
    print "player: ", players

# Game logic based on if the stackees have correctly scanned
def do_client_response(sock, msg):
    global client_msgs_recieved, round_number, round_passable
    # Add in the new info
    round_passable &= msg
    client_msgs_recieved += 1
    # See if we're done
    if client_msgs_recieved != NUM_COMPUTERS:
        return
    else:
        if round_passable:
            if round_number == ROUNDS_TOTAL:
                broadcast_data("GAME_DONE")
            else: 
                round_start()
        else:   
            broadcast_data("KEEP_SCANNING")
            client_msgs_recieved = 0
            round_passable = True

# Start a new round of the game by sending out the order of players at
# each client
def round_start():    
    # Bookkeeping
    global round_number, client_msgs_recieved, round_passable, players
    round_number += 1
    client_msgs_recieved = 0
    round_passable = True
    # Shuffle and send out
    random.shuffle(players)
    idx = 0
    for sock, num in num_client_players.items():
        curr_players = players[idx:idx+num]
        idx += num
        send_data(sock, curr_players) 

# Add a client to the game and check to see if all clients have existed
def add_client(sock, msg):
    if sock not in num_client_players:
        num_client_players[sock] = msg
        if sum(num_client_players.values()) == NUM_PLAYERS:
            display_game_can_start()
            round_start()
    print num_client_players

# Parses the messages and... does stuff.
# In this case, it decides what type of message was recieved, and behaves
# accordingly. 
def do_stuff(sock, msg):
    print "from server", sock, msg
    if type(msg) == int:
        add_client(sock, msg)
    if type(msg) == bool:
        do_client_response(sock, msg)

# Loads in the initial set of players
def add_new_player(player):
    if player not in players:
        players.append(player)
        print "added player:", player
        if len(players) == NUM_PLAYERS:
            round_start()

################ Communication related functions

connection_list = []

server_socket = ""

# Function to send data over a socket
def send_data(socket, message):
    global server_socket
    if socket != server_socket and socket != sys.stdin:
        try:
            socket.send(json.dumps(message))
            print "server sent message: ", message
        except Exception, err:
            print Exception, err
            print "Data send error on socket: ", socket
            print connection_list
            print "server_socket", server_socket
            exit_safely()

# Send stuff to all clients
def broadcast_data(message):
    for socket in connection_list:
        send_data(socket, message)

# close all connections
def exit_safely():
    for connection in connection_list:
        connection.close()
    sys.exit()

# Tells this guy to be the server and to listen for messages
def be_server():
    # Make our socket
    global server_socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("127.0.0.1", PORT))
    server_socket.listen(10)

    # Add server to list of readable connections
    connection_list.append(server_socket)

    # Add stdin so that we can read in newly scanned RFID cards
    connection_list.append(sys.stdin)


    while True:
        # get all sockets which could be read through
        read_sock, write_sock, err_sock = select.select(connection_list,[],[])
        for sock in read_sock:
            # New player
            if sock == sys.stdin:
                msg = sys.stdin.readline()
                add_new_player(msg)
            # new connection
            elif sock == server_socket:
                sockfd, addr = server_socket.accept()
                connection_list.append(sockfd)
                print "client (%s, %s) connected" % addr
            # Got incoming message
            else:
                try:
                    data = sock.recv(RECV_BUFFER)
                    if data != '': 
                        do_stuff(sock, json.loads(data))
                except Exception, err:
                    print Exception, err
                    print "Client (%s, %s) is offline" % addr
                    exit_safely()
    close_all_connections()


########### Main loop
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Usage: server.py [number of computers] [number of stackers]"
        sys.exit()
    NUM_COMPUTERS = int(sys.argv[1])
    NUM_PLAYERS = int(sys.argv[2])
    global players
    players = [ player_dict[x] for x in range(1, NUM_PLAYERS)]
    be_server()
