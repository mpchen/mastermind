#not /usr/bin/env python

# This can only be turned on in LAN mode
USE_DISPLAY = False



from PIL import Image
import sys
import subprocess

WIDTH = 200
HEIGHT = 200

R_HEIGHT = 0 * HEIGHT
Y_HEIGHT = 1 * HEIGHT
G_HEIGHT = 2 * HEIGHT
GR_HEIGHT = 3 * HEIGHT


green_on = Image.open("images/green_on.png").resize((WIDTH, HEIGHT))
green_off = Image.open("images/green_off.png").resize((WIDTH, HEIGHT))
yellow_on = Image.open("images/yellow_on.png").resize((WIDTH, HEIGHT))
yellow_off = Image.open("images/yellow_off.png").resize((WIDTH, HEIGHT))
red_on = Image.open("images/red_on.png").resize((WIDTH, HEIGHT))
red_off = Image.open("images/red_off.png").resize((WIDTH, HEIGHT))
grey_on = Image.open("images/grey_on.png").resize((WIDTH, HEIGHT))
grey_off = Image.open("images/grey_off.png").resize((WIDTH, HEIGHT))

all_grey_off = '' # Fill this in during the Init (need to know # players)
all_grey_on = ''  # Same as above

curr_image = '' # Update this as necessary

make_show = ''
if "linux" in sys.platform:
    make_show = "xdg-open"
if "darwin" in sys.platform:
    make_show = "open"

DISPLAY_FILE = "current.png"
image_process = ''

######## Since there's no easy way to close an image window, we make
# another process for it.

# Is this hacky? Hell yes it is. 

def save_img_to_display(img):
    img.save(DISPLAY_FILE, "PNG")

def show_display_image():
    global image_process
    image_process = subprocess.Popen([make_show, DISPLAY_FILE])

def end_display_image():
    image_process.terminate()
    image_process.kill()

def kill_old_show_new(img):
    end_display_image()
    save_img_to_display(img)
    show_display_image()


######## Image logic


def display_new_results(results):
    if not USE_DISPLAY:
        return
    temp = all_grey_on.copy()
    for i in range(results):
        if results[i][0] == True:
            temp.paste(red_on, (i * WIDTH, R_HEIGHT))
        if results[i][1] == True:
            temp.paste(yellow_on, (i * WIDTH, Y_HEIGHT))
        if results[i][0] == True:
            temp.paste(green_on, (i * WIDTH, G_HEIGHT))
    kill_old_show_new(temp)

    return

def display_game_done():
    # TODO: Make this pretty?
    print "CONGRATS. YOU'RE DONE."

def display_new_scan(show_up_to):
    if not USE_DISPLAY:
        return
    temp = all_grey_off.copy()
    for i in xrange(0, show_up_to * WIDTH, WIDTH):
        temp.paste(grey_on, (i,GR_HEIGHT))
    kill_old_show_new(temp) 

def display_set_up(num_players):
    if not USE_DISPLAY:
        return

    global all_grey_off, all_grey_on
    all_grey_off = Image.new('RGB', (num_players * WIDTH, 4 * HEIGHT))
    for i in xrange(0, num_players * WIDTH, WIDTH):
        all_grey_off.paste(red_off, (i, R_HEIGHT))
        all_grey_off.paste(yellow_off, (i, Y_HEIGHT))
        all_grey_off.paste(green_off, (i, G_HEIGHT))
        all_grey_off.paste(grey_off, (i, GR_HEIGHT))

    all_grey_on = all_grey_off.copy()
    for i in xrange(0, num_players * WIDTH, WIDTH):
        all_grey_on.paste(grey_on, (i, GR_HEIGHT))

    save_img_to_display(all_grey_off) 
    show_display_image()





